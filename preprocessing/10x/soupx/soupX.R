# ==================================================
# Title: "Ambient RNA correction with SoupX"
# Author: "Maksim Kholmatov"
# ==================================================


# ---- Setup ----
## ---- Load Libraries ----
library("tidyverse")
library("here")
library("SoupX")
library("futile.logger")
library("sparseMatrixStats")
library(future.apply)

## ---- Configure parallelization ----

plan("multisession", workers = 10)
set.seed(68576)


# ---- Function to correct counts ----

correct_counts <- function(library, inputDir) {
  dir.create(here("output", library, "filtered_feature_bc_matrix/"), recursive = T)
  
  file.copy(from = here(inputDir, library, "outs/filtered_feature_bc_matrix"), to = here("output", library), overwrite = T, recursive = T)
  
  sc = load10X(here(inputDir, library, "outs"))
  sc = autoEstCont(sc)
  out = adjustCounts(sc, roundToInt = T) # as recommended by SCTransform maintainer
  eff_rho = (1 - colSums2(out)/colSums2(sc$toc))
  names(eff_rho) <- colnames(out)
  saveRDS(eff_rho, file =  here("output", library, "effective_rho.rds"))
  saveRDS(sc, file =  here("output", library, "soup_channel.rds"))
  writeMMgz(
    x = out, 
    file = here("output", library, "filtered_feature_bc_matrix", "matrix.mtx.gz"),
    t = 8)
}


#  ---- Apply to all libraries  ----

rootDir = "../cellranger/"
libraries <- dir(rootDir, pattern = "SIGA*")

future_sapply(libraries, partial(correct_counts, inputDir=rootDir), future.seed=TRUE)