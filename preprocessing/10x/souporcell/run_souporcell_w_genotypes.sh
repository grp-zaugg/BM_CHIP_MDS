export APPTAINERENV_RUST_BACKTRACE=full

args=( $@ )
library="${args[0]}"
k="${args[1]}"
names=$(echo ${args[2]} | sed -e 's/,/ /g')

echo $library
echo $k
echo $names

apptainer exec -B "${PWD}" ./souporcell_latest.sif souporcell_pipeline.py \
        -i "./bams_joined/${library}/possorted_genome_bam.bam" \
        -b "./bams_joined/${library}/barcodes.tsv.gz"  \
        -f ./fasta/genome.fa \
        -t 16 \
        -o "./${library}_w_genotypes_imputedEUR" \
        --known_genotypes "./genotypes/imputation/results/AllAutosomesEUR_GRCh38.vcf" \
        -k "${k}" \
        --ploidy 2 \
        --known_genotypes_sample_names ${names}
