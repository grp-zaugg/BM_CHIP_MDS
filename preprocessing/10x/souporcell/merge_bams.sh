#! /bin/sh

while read -r line
do
        echo $line
        library=$(echo ${line} | sed -e "s/ /_/g")

        mkdir -p "./bams_joined/${library}"
        n=1
        for sub_library in ${line[@]}
        do
                mkfifo "./bams_joined/${library}/${sub_library}_tmp.bam"
                mkfifo "./bams_joined/${library}/${sub_library}_barcodes"
                if [ "$n" -gt 1 ]
                then
                        samtools view -h "../cellranger/${sub_library}/outs/possorted_genome_bam.bam" | sed -e "s/\(CB:Z:[AGTC]\{16\}\)-1/\1-${n}/" | samtools view -u > "./bams_joined/${library}/${sub_library}_tmp.bam" &
                        zcat "../cellranger/${sub_library}/outs/filtered_feature_bc_matrix/barcodes.tsv.gz" | sed -e "s/-1/-${n}/" > "./bams_joined/${library}/${sub_library}_barcodes" &
        
                else
                        samtools view -h "../cellranger/${sub_library}/outs/possorted_genome_bam.bam" | samtools view -u > "./bams_joined/${library}/${sub_library}_tmp.bam" &
                        zcat "../cellranger/${sub_library}/outs/filtered_feature_bc_matrix/barcodes.tsv.gz"  > "./bams_joined/${library}/${sub_library}_barcodes" &
        
                fi
        
                n=$(($n+1))
        done
        
        barcodes=$(ls ./bams_joined/${library}/*_barcodes)
        bams=$(ls ./bams_joined/${library}/*_tmp.bam)
        #echo $barcodes
        cat $barcodes | pigz -c -p 12 --best > "./bams_joined/${library}/barcodes.tsv.gz" &
        samtools merge -l 9 -O BAM -@ 12 "./bams_joined/${library}/possorted_genome_bam.bam" ${bams}
        samtools index -@ 16 "./bams_joined/${library}/possorted_genome_bam.bam"
        wait
        
        rm ./bams_joined/${library}/*_tmp.bam
        rm ./bams_joined/${library}/*_barcodes

done < ./libraries_to_merge.tsv



