#! /bin/sh


cd ./cellranger

cellranger count --id=$1 \
    --transcriptome="[CELLRANGER REFERENCE]/refdata-gex-GRCh38-2020-A" \
        --fastqs="../../../data/FASTQ/10x/" \
        --sample=$1 \
        --localcores=32 \
        --localmem=128
