configfile: "config.yaml"

fastq_dir=config["inputs"]["fastq_dir"]

PLATES = [1,2,3,4,5]
samples, libraries, reads = glob_wildcards(f'{fastq_dir}' + "{id, [^.]+}_L00{lib}_R{read}_001.fastq.gz")

samples = set(samples)
libraries = set(libraries)

localrules: all

rule all:
    input:
        expand("{sample}.done", sample=samples),
        "unloading.done"

rule run_dropest:
    input:
        bam=f'{config["outputs"]["output_dir"]}' + "STAR/" + "{sample}/Aligned.sortedByCoord.filtered.bam"
    params:
        dropEstConf=config["dropEst"]["config"],
        genesGTF=config["dropEst"]["GTF"],
        outdir=config["outputs"]["output_dir"]
    envmodules:
        "dropEst"
    output:
        rds=f'{config["outputs"]["output_dir"]}' + "/dropEst/{sample}/cell.counts.rds",
        logs=f'{config["outputs"]["output_dir"]}' + "/dropEst/{sample}/_est_main.log",
        done=temp(touch("{sample}.done"))
    shell:
        """
        dropest -f -g {params.genesGTF} \
            -c {params.dropEstConf} \
            {input.bam} \
            -l '{params.outdir}/dropEst/{wildcards.sample}/' \
            -o '{params.outdir}/dropEst/{wildcards.sample}/cell.counts.rds'
        """


rule filter_empty_CB:
    input:
        bam=f'{config["outputs"]["output_dir"]}' + "STAR/" + "{sample}/Aligned.sortedByCoord.out.bam"
    envmodules:
        "SAMtools"
    output:
        bam=f'{config["outputs"]["output_dir"]}' + "STAR/" + "{sample}/Aligned.sortedByCoord.filtered.bam"
    shell:
        """
        samtools view -h {input.bam} | \
            grep -v 'CB:Z:-' | \
            samtools view -b -h -@ {threads} -o {output.bam} -
        """

rule unload_star_genome:
    input:
        bam=expand(f'{config["outputs"]["output_dir"]}' + "STAR/" + "{sample}/Aligned.sortedByCoord.out.bam", sample=samples),
        genome_loaded="loading.done"
    envmodules:
        "STAR"
    params:
        genomeDir=config["STAR"]["genomeDir"],
    output:
        temp(touch("unloading.done"))
    shell:
        """
        /home/kholmato/.builds/STAR-2.7.10b/source/STAR --genomeLoad Remove --genomeDir {params.genomeDir} \
                --outFileNamePrefix $TMPDIR/STAR_unload/
        """

rule load_star_genome:
    input:
        genomeDir=config["STAR"]["genomeDir"],
    output:
        temp(touch("loading.done"))
    envmodules:
        "STAR"
    shell:
        """
        /home/kholmato/.builds/STAR-2.7.10b/source/STAR --genomeLoad LoadAndExit --genomeDir {input.genomeDir} \
                --outFileNamePrefix $TMPDIR/STAR_load/
        """

rule run_star:
    input:
        r1=expand(f'{fastq_dir}' + "{{sample}}_L00{lib}_R1_001.fastq.gz", lib=libraries),
        r2=expand(f'{fastq_dir}' + "{{sample}}_L00{lib}_R2_001.fastq.gz", lib=libraries),
        CB_whitelist=config["inputs"]["CB_whitelist"],
        genome_loaded="loading.done"
    params:
        genomeDir=config["STAR"]["genomeDir"],
        outdir=config["outputs"]["output_dir"],
        reads1=lambda w, input: ",".join(input.r1),
        reads2=lambda w, input: ",".join(input.r2)
    envmodules:
        "STAR"
    threads:
        config["STAR"]["threads"]
    output:
        bam=f'{config["outputs"]["output_dir"]}' + "STAR/" + "{sample}/Aligned.sortedByCoord.out.bam"
    shell:
        """
            /home/kholmato/.builds/STAR-2.7.10b/source/STAR \
                --runThreadN {threads} \
                --genomeLoad LoadAndKeep \
                --genomeDir {params.genomeDir} \
                --readFilesIn {params.reads2} {params.reads1} \
                --readFilesCommand gunzip -c \
                --soloType CB_UMI_Simple \
                --soloCBwhitelist {input.CB_whitelist} \
                --soloBarcodeReadLength 0 \
                --soloUMIstart 1 \
                --soloUMIlen 6 \
                --soloCBstart 7 \
                --soloCBlen 8 \
                --soloCellFilter  EmptyDrops_CR \
                --outSAMattributes NH HI nM AS CR UR CY UY CB UB GX GN cN \
                --outSAMtype BAM SortedByCoordinate \
                --soloFeatures Gene GeneFull \
                --clipAdapterType CellRanger4 --outFilterScoreMin 30 --soloCBmatchWLtype 1MM_multi_Nbase_pseudocounts --soloUMIfiltering MultiGeneUMI --soloUMIdedup NoDedup \
                --outFileNamePrefix {params.outdir}/STAR/{wildcards.sample}/ \
                --outBAMsortingThreadN {threads} \
                --limitBAMsortRAM 16000000000 \
                --outTmpDir $TMPDIR/STAR_{wildcards.sample}
        """
