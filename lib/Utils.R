read_samples <- function(d) {
  samples = dir(d, pattern = "SIG*")
  names(samples) <- samples
  samples <- samples[
    c(
      "SIGAC12", 
      "SIGAD8",
      "SIGAD9",
      "SIGAD10",
      "SIGAD11", 
      "SIGAD12", 
      "SIGAE1", 
      "SIGAE2",
      "SIGAE3",
      "SIGAE4")
  ]
  samples
}


read_seurats_chunked <- function(samples, soup_dir, cr_dir, shared_samples = NULL, metadata = NULL, scDblFinder = NULL) {
  require(future.apply)
  seurats = future_sapply(samples, FUN = function(x) {
    suppressMessages({
      md = read_tsv(here(
        soup_dir,
        paste0(paste0(x, collapse = "_"), "_w_genotypes_imputedEUR"),
        "clusters.tsv")
        ) %>% select(barcode, status, assignment) %>%
        mutate(assignment = as.numeric(assignment))
    })

    seurats = future_sapply(seq_along(x), function(n) {
      seurat = Read10X(here(cr_dir, x[n],"filtered_feature_bc_matrix")) %>%
        CreateSeuratObject(project = "MDS", min.cells = 3, min.features = 500,
                           meta.data = md %>%
                             filter(grepl(paste0("[ACGT]{16}-", n), barcode)) %>%
                             mutate(barcode = gsub(n, "1", barcode)) %>%
                             column_to_rownames("barcode")
                           )

      seurat$library <- x[n]

      if (!is.null(shared_samples)) {
        shared = read_tsv(shared_samples, col_names = c("library", "PatientID"))
        seurat$PatientID = NA

        shared$PatientID[which(shared$library == paste0(x, collapse = "_"))] %>%
          str_split(",") %>% .[[1]] -> patient

        seurat$PatientID = patient[seurat$assignment +1]

        seurat$population = NA

        seurat$population[seurat$library %in% c(
          "SIGAC12",
          "SIGAD9",
          "SIGAD11",
          "SIGAE1",
          "SIGAE3")] = 'Stroma'
        seurat$population[seurat$library %in% c(
          "SIGAD8",
          "SIGAD10",
          "SIGAD12",
          "SIGAE2",
          "SIGAE4")] = 'T+HSPC'
      }
      if (!is.null(metadata)) {
        met = read_tsv(metadata, show_col_types = F, progress = F)
        nmd <- left_join(seurat@meta.data, met)

        seurat$SampleID <- NA
        seurat$Sex <- NA
        seurat$Condition <- NA

        seurat$SampleID <- nmd$SampleID
        seurat$Sex <- nmd$Sex
        seurat$Condition <- nmd$Condition

        seurat$ConditionID <- paste0(seurat$Condition, "_", seurat$SampleID)
        seurat$ConditionID[seurat$ConditionID == "NA_NA"] = NA
      }
      if (!is.null(scDblFinder)) {
        met = read_tsv(here(scDblFinder, paste0(x[n], "_celldata.tsv.gz")), show_col_types = F, progress = F)
        nmd <- left_join(seurat@meta.data %>% as_tibble(rownames = "barcode"), met, by = "barcode")

        seurat$scDblFinder.class <- NA
        seurat$scDblFinder.score <- NA

        seurat$scDblFinder.class <- nmd$scDblFinder.class
        seurat$scDblFinder.score <- nmd$scDblFinder.score
      }
      seurat
    }, USE.NAMES = T, future.seed=TRUE)
    names(seurats) <- x
    seurats
  }, USE.NAMES = F, future.seed=TRUE) %>% flatten()
  seurats
}
